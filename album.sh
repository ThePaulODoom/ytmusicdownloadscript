#!/bin/bash
shopt -s extglob
[ "$2" = "auto" ] && PLAYLIST='%(autonumber)03d' || PLAYLIST='%(playlist_index)s'
#playtitle=`curl "$1" | perl -MHTML::Entities -pe 'decode_entities($_);' | grep -Eo '<meta property="og:title" content=.+">' | cut -d'=' -f3 | tr -d \" | tr -d \>`
# mkdir -v "$playtitle"
# cd "$playtitle"
yt-dlp "$1" --embed-metadata --embed-thumbnail -o "%(artist)s/%(playlist_title)s/$PLAYLIST - %(title)s.%(ext)s" -f m4a --no-cache-dir --parse-metadata "$PLAYLIST:%(meta_track)s" --ppa "ffmpeg:-metadata date="
# yt-dlp "$1" --max-downloads 1 --skip-download --write-thumbnail  -o "Folder.%(ext)s" --no-cache-dir
# test -e "Folder.png" || ffmpeg -y -i Folder* Folder.png
# rm Folder.!(png)
# for i in [0-9]*; do ffmpeg -loglevel 0 -i "$i" -metadata date= -metadata track=$(echo $i | cut -d- -f 1) -c copy "$(echo $i | cut -d- -f 2-)"; rm "$i"; done
